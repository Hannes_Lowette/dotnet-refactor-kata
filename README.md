# Gilded Rose refactoring kata - .NET  #

This is an exercise in clean code practices. The goal is to prove that one can take the provided code and come to a solution that is easier to read and maintain.

### What is expected? ###

You are given the code in this repository. It is bad. We don't know how it came into existence, all we know is that the auther worked off [these requirements](requirements.txt).

We expect you to give us a better version of this code base. We need something readble and maintainable. How you go about this is up to you. 

Some things we would like to see:

* The final result
* Which steps you took to come to this solution
* Working tests

### Where does this kata come from? ###

The original kata comes from [Terry Hughes](https://twitter.com/TerryHughes), and can be found on GitHub here: [https://github.com/NotMyself/GildedRose](https://github.com/NotMyself/GildedRose).

[Emily Bache](https://twitter.com/emilybache) refined it and translated it to a number of programming languages. Her repo can be found here: [https://github.com/emilybache/GildedRose-Refactoring-Kata](https://github.com/emilybache/GildedRose-Refactoring-Kata)